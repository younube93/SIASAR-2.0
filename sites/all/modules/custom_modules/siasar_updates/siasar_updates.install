<?php

/**
 * Initial update hook to enable adminrole
 * It renames old "Administrator" to "administrator" role, for the module to work.
 * https://gitlab.com/isegura/SIASAR-2.0/issues/9
 */
function siasar_updates_update_7100(&$sandbox) {
  $role = user_role_load_by_name('Administrator');
  $role->name = 'administrator';
  user_role_save($role);

  $modules = array(
    'adminrole',
  );

  $modules_enabled = module_enable($modules, TRUE);
  if (!$modules_enabled) {
    throw new DrupalUpdateException('Something went wrong; A dependency was missing.');
  }
}

/**
 * Issue 153: Duplicated Field Collection via REST API (POST request)
 * https://gitlab.com/Admin_Siasar/SIASAR-2.0/issues/153
 * Enables our own custom endpoint for field collections, letting us workaround the issue.
 */
function siasar_updates_update_7101(&$sandbox) {
  $modules = array(
    'services_field_collection',
  );
  $modules_enabled = module_enable($modules, TRUE);
  if (!$modules_enabled) {
    throw new DrupalUpdateException('Something went wrong; A dependency was missing.');
  }
}

/**
 * Issue 169: Refactor Features. Uninstall old ones.
 * https://gitlab.com/Admin_Siasar/SIASAR-2.0/issues/169
 */
function siasar_updates_update_7102(&$sandbox) {
  $modules = array(
    'feature_sistema',
    'feaute_prestador_servicio',
    'features_pat',
  );
  $result = module_disable($modules, TRUE);
  $result = drupal_uninstall_modules($modules, TRUE);
}

/**
 * Issue 170: Add environment color indicator on admin bar to avoid confusion between local, staging and production. 
 * https://gitlab.com/Admin_Siasar/SIASAR-2.0/issues/170
 * Enables environment_indicator.module and its asociated Feature.
 */
function siasar_updates_update_7103(&$sandbox) {
  $modules = array(
    'environment_indicator',
    'feature_siasar_environment_indicator',
  );
  $modules_enabled = module_enable($modules, TRUE);
}

/**
 * Issue isegura-10: removing hack on Services module while fixing problem with Geofield binary output.
 * https://gitlab.com/isegura/SIASAR-2.0/issues/10
 */
function siasar_updates_update_7104(&$sandbox) {
  $modules = array(
    'siasar_services_geofield',
  );
  $modules_enabled = module_enable($modules, TRUE);
  if (!$modules_enabled) {
    throw new DrupalUpdateException('Something went wrong; A dependency was missing.');
  }
}

/**
 * Issue 186: Cambio de valores retornados por la API REST para los campos de referencia a entidades (Sistema, PS, PAT y Comunidad)
 * https://gitlab.com/Admin_Siasar/SIASAR-2.0/issues/186
 */
function siasar_updates_update_7105(&$sandbox) {
  $modules = array(
    'siasar_services_field_collection_alter',
  );
  $modules_enabled = module_enable($modules, TRUE);
  if (!$modules_enabled) {
    throw new DrupalUpdateException('Something went wrong; A dependency was missing.');
  }
}

/**
 * Issue 190: disabling unused modules, reverting Feature
 * https://gitlab.com/Admin_Siasar/SIASAR-2.0/issues/190
 */
function siasar_updates_update_7106(&$sandbox) {
  $modules = array(
    'i18n_contact',
    'contact',
    'comment',
  );
  $result = module_disable($modules, TRUE);

  features_revert_module('feature_siasar_environment_indicator');
}
